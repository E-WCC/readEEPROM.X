/*! \file  initialization.c
 *
 *  \brief Initialization for readEEPROM
 *
 *
 *  \author jjmcd
 *  \date December 31, 2015, 1:54 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/24FC128.h"
#include "../include/LCD.h"


/*! initialization - Initialize hardware for readEEPROM             */
/*! initialization() sets the port direction, initializes the
 *  LCD, and initializes the I2C bus.
 */
void initialization(void)
{
  /* Turn on LED1 during initialization                             */
  _TRISB4 = 0;          /* LED1 pin output                          */
  _LATB4 = 1;           /* LED1 on                                  */
  Delay_ms(200);        /* Give it a tiny time                      */
  
  /* Initialize the LCD, then display progress message              */
  LCDinit();            /* Initialize the LCD                       */
  LCDclear();           /* Clear the LCD                            */
  LCDhome();            /* Might not be power on                    */
  Delay_ms(200);        /* More delay                               */
  LCDputs("Init");      /* Let user know we are initializing I2C    */

  /* Initialize the I2C and again another message                   */
  I2Cinit();            /* Initialize the I2C peripheral            */
  LCDputs(" wait");     /* Another progress message                 */
  Delay_ms(1000);       /* Another delay to read                    */
  
  /* Turn off LED1 at end of initialization                         */
  _LATB4 = 0;           /* Turn off LED1                            */
}
