/*! \file  adjustClock.c
 *
 *  \brief Set the oscillator PLL chain
 *
 *
 *  \author jjmcd
 *  \date December 30, 2015, 8:38 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! adjustClock - Set the PIC clock to 70 MIPS              */

/*! adjustClock() - set the oscillator chain
 *  adjustClock() set the registers in the oscillator chain
 *  to result in an instruction rate of 70 MIPS.            */
void adjustClock(void)
{
  /* The FRC oscillator provides a 7.37 MHz clock.  The PLL
   * chain gives a number of divide and multiply options
   * to allow selecting an appropriate clock.              */
  
  /* The Fast RC post scaler can divide the 7.37 MHz clock
   * by a umber between 1 and 128.  FRCdiv is set to 0
   * to divide by 1, 1 to divide by 2, 2 to divide 
   * by 4, etc.                                             */
  CLKDIVbits.FRCDIV = 0;    /* Divide by 1 = 7.37MHz        */
  
  /* The PLL prescaler can divide by a number from 2 to
   * 33.  Setting 0 divides by 2, 1 by 3, etc.              */
  CLKDIVbits.PLLPRE = 0;    /* Divide by 2 = 3.685 MHz      */
  
  /* The PLL feedback divider causes the frequency used to
   * be multiplied, by dividing the feedback.  Choices are
   * 2 (register value of 0) to 512 (register value of
   * 511).  The result must end up with a final frequency
   * within the processor specifications (<=140).           */
  PLLFBD = 74;              /* Multiply by 76 = 280         */
  
  /* The PLL postscaler can divide the result by a value
   * between 2 and 8, with a register value of 0 dividing
   * by 2, 1 by 4, and 3 by 8.  A register value of 2 is
   * not permitted.                                         */
  CLKDIVbits.PLLPOST = 0;   /* Divide by 2 = 140            */
  
  /* Each instruction takes 2 clock cycles, so the 140 MHz
   * clock results in an instruction rate of 70 MIPS        */
}
