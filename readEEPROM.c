/*! \file  readEEPROM.c
 *
 *  \brief Read the contents of the EEPROM and display
 *
 *  \author jjmcd
 *  \date December 4, 2015, 4:39 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <stdio.h>
#include "../include/24FC128.h"
#include "../include/LCD.h"

// Fast RC oscillator with PLL
#pragma config FNOSC = FRCPLL
// Watchdog timer off
#pragma config FWDTEN = OFF
// Deadman timer off
#pragma config DMTEN = DISABLE

/* Constants for the application                                */
/*! I2C address of the serial EEPROM                            */
#define EEPROM_ADDR 0xaa
/*! Address of the message within the serial EEPROM             */
//#define MSG_START 0xc00
#define MSG_START 0x0c00

/* Function prototypes                                          */
/*! adjustClock - Set the PIC clock to 70 MIPS                  */
void adjustClock( void );
/*! initialization - Initialize hardware for readEEPROM         */
void initialization( void );

/*! main - Read the contents of the EEPROM and display          */

/*! The 24FC128 serial EEPROM at I2C address 0xaa is assumed to have
 *  text terminated with a null.  The text is displayed on the LCD
 *  with little formatting.
 */
int main(void)
{
  char cLetter;     /* Letter to display                        */
  int nAddr;        /* Address within serial EEPROM             */
  int nPos;         /* Position on LCD                          */
  char szWork[32];
  
  /* Set the processor clock for a 70 MIPS instruction rate     */
  adjustClock();
  /* Initialize the hardware                                    */
  initialization();

  /* We will now read characters from the EEPROM and display
   * them on the LCD.  We count the position on the EEPROM
   * independently of the position on the LCD since we want
   * to alternate LCD lines every 16 characters.
   */
  while (1)
    {
      /* Start message and LCD display                              */
      nAddr = MSG_START;            /* Start of message in EEPROM   */
      cLetter = 'Z';                /* Can't have cLetter being null*/
      nPos = 0;                     /* Position on LCD              */

      /* We will continue until we encounter a NULL character, 
       * after which we will fall out of the loop below and the
       * outer loop will restart at the beginning of the
       * message
       */
      while (cLetter)               /* Until a null reached         */
        {
          /* Grab the character from the serial EEPROM              */
          cLetter = read24FC128(EEPROM_ADDR, nAddr);
          if (cLetter)              /* Only display if not null     */
            {
              LCDposition(nPos);    /* Position LCD                 */
              LCDletter(cLetter);   /* Show the letter              */
              nAddr++;              /* Next EEPROM address          */
              nPos++;               /* Next LCD position            */
              if (nPos == 16)       /* End of line 1?               */
                nPos = 0x40;        /* Yes, go to line 2            */
              if (nPos == 0x50)     /* End of line 2?               */
                nPos = 0;           /* Yes, go to line 1            */
              Delay_ms(30);         /* Slow it down a bit           */
#ifdef ADDRESS_DISPLAY
              if ( (nAddr % 0x100)==0 )
                {
                  LCDposition(0x44);
                  sprintf(szWork," 0x%04x ",nAddr);
                  LCDputs(szWork);
                  Delay_ms(500);
                }
#endif
            }
        }
      /* At the end of the message, display a few blanks to
       * separate the end from the beginning, turn on LED1
       * to notify the user, then wait a short while before
       * restarting.
       */
      LCDputs("               ");   /* Clear out a bunch when done  */
      _LATB4 = 1; /* LED1 on                      */
      LCDposition(0x44);
      sprintf(szWork, " 0x%04x ", nAddr);
      LCDputs(szWork);
      Delay_ms(2000); /* Wait a bit before restarting */
      _LATB4 = 0;                   /* LED1 off                     */
  }

  return 0;
}
